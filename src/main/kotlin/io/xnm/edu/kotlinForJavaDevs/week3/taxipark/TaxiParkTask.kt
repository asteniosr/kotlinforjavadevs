package io.xnm.edu.kotlinForJavaDevs.week3.taxipark


/*
 * Task #1. Find all the drivers who performed no trips.
 */
fun TaxiPark.findFakeDrivers(): Set<Driver> =
    allDrivers.minus(trips.groupBy { it.driver }.keys)

/*
 * Task #2. Find all the clients who completed at least the given number of trips.
 */
fun TaxiPark.findFaithfulPassengers(minTrips: Int): Set<Passenger> =
    allPassengers.filter {
            passenger -> passenger.isFaithful(this.trips, minTrips)
    }.toSet()

private fun Passenger.isFaithful(trips: List<Trip>, minTrips: Int) =
    trips.filter { it.passengers.contains(this) }.count() >= minTrips


/*
 * Task #3. Find all the passengers, who were taken by a given driver more than once.
 */
fun TaxiPark.findFrequentPassengers(driver: Driver): Set<Passenger> =
    allPassengers.filter { passenger ->  passenger.isFrequent(this.trips) }.toSet()

private fun Passenger.isFrequent(trips: List<Trip>) =
    !trips.filter {
        it.passengers.contains(this)
    }.groupBy {
        it.driver
    }.filter {
            (_,t) -> t.size > 1
    }.isEmpty()

/*
 * Task #4. Find the passengers who had a discount for majority of their trips.
 */
fun TaxiPark.findSmartPassengers(): Set<Passenger> =
    allPassengers.filter { passenger -> passenger.isSmart(this.trips) }.toSet()

private fun Passenger.isSmart(trips: List<Trip>): Boolean {
    val (withDiscount, withoutDiscount) = trips.filter {
        it.passengers.contains(this) }.partition { it.discount != null }

    return withDiscount.size > withoutDiscount.size
}

/*
 * Task #5. Find the most frequent trip duration among minute periods 0..9, 10..19, 20..29, and so on.
 * Return any period if many are the most frequent, return `null` if there're no trips.
 */
fun TaxiPark.findTheMostFrequentTripDurationPeriod(): IntRange? {
    return kotlin.TODO()
}

/*
 * Task #6.
 * Check whether 20% of the drivers contribute 80% of the income.
 */
fun TaxiPark.checkParetoPrinciple(): Boolean {
    kotlin.TODO()
}
