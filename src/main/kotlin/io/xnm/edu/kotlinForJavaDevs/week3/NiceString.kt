package io.xnm.edu.kotlinForJavaDevs.week3

fun String.isNice(): Boolean {
    var count =  0

    if (noBaBeBu())
        count++
    println("$count")

    if (atLeastThreeVowels())
        count++
    println("$count")

    if (containsDoubleLetter())
        count++
    println("$count")

    return count > 1
}

private fun String.noBaBeBu() = listOf("ba","be","bu").stream().map{ this.contains(it) }.allMatch { e -> !e }


private fun String.atLeastThreeVowels() = this.count { c -> c.isVowel() } > 2

private fun String.containsDoubleLetter() =
    mapIndexed { index, c -> index < this.length - 1 && c == this[index + 1] }.any{ e -> e }

private fun Char.isVowel() = this.isLetter() && this.toLowerCase() in "a,e,i,o,u"
