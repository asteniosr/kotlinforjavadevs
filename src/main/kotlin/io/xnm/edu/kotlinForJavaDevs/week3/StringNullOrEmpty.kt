package io.xnm.edu.kotlinForJavaDevs.week3

fun String?.isEmptyOrNull(): Boolean {

     return this == null || this.isEmpty()
}
