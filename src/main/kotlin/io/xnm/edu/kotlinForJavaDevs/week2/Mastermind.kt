package io.xnm.edu.kotlinForJavaDevs.week2

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)

fun evaluateGuess(secret:String, guess: String): Evaluation{

    var localSecret = ""
    var localGuess = ""

    for (i in 0 until secret.length){
        if (secret[i] != guess[i]) {
            localSecret = localSecret.plus(secret[i])
            localGuess = localGuess.plus(guess[i])
        }
    }

    localGuess.forEach {
        run {
            val lastIndexOf = localSecret.lastIndexOf(it)
            if (lastIndexOf > -1) {
                localSecret = localSecret.removeRange(lastIndexOf, lastIndexOf + 1)
            }
        }
    }

    var rightPosition = guess.length - localGuess.length
    var wrongPosition = localGuess.length - localSecret.length

    return Evaluation(rightPosition,wrongPosition)
}
