package io.xnm.edu.kotlinForJavaDevs.week2

class CheckIdentifier {

    fun isValidIdentifier(s: String): Boolean {
        fun allCharsOrNums(s: String): Boolean {
            s.forEach { when {
                !it.isLetter() && !it.isDigit() -> return false
            }}
            return true
        }

        fun checkStaringChar(c: Char): Boolean {
            return c == '_' || c.isLowerCase()
        }

        return s.isNotEmpty() && checkStaringChar(s[0]) && allCharsOrNums(s.drop(1))
    }
}
