package io.xnm.edu.kotlinForJavaDevs.week2

class SumAsExtension {
    fun List<Int>.sum(): Int {
        var result = 0
        for (i in this) {
            result += i
        }
        return result
    }
}
