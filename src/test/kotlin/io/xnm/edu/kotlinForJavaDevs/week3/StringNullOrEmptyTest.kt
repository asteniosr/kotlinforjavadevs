package io.xnm.edu.kotlinForJavaDevs.week3

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.jupiter.api.Test

class StringNullOrEmptyTest {

    val s1: String? = null
    val s2: String? = ""
    val s3 = "   "

    @Test
    fun testS1() = assertTrue(s1.isEmptyOrNull())

    @Test
    fun testS2() = assertTrue(s2.isEmptyOrNull())

    @Test
    fun testS3() = assertFalse(s3.isEmptyOrNull())
}
