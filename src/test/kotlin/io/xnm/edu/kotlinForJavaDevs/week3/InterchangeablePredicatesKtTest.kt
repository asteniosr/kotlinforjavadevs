package io.xnm.edu.kotlinForJavaDevs.week3

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class InterchangeablePredicatesKtTest {

    private val list1 = listOf(1, 2, 3)
    private val list2 = listOf(0, 1, 2)

    @Test
    fun allNonZero() = assertTrue(list1.allNonZero())

    @Test
    fun allNonZero1() = assertTrue(list1.allNonZero1())

    @Test
    fun allNonZero2() = assertTrue(list1.allNonZero2())

    @Test
    fun alContainsZero() = assertFalse(list1.containsZero())

    @Test
    fun alContainsZero1() = assertFalse(list1.containsZero1())

    @Test
    fun alContainsZero2() = assertFalse(list1.containsZero2())

    @Test
    fun allNonZero20() = assertFalse(list2.allNonZero())

    @Test
    fun allNonZero21() = assertFalse(list2.allNonZero1())

    @Test
    fun allNonZero22() = assertFalse(list2.allNonZero2())

    @Test
    fun alContainsZero20() = assertTrue(list2.containsZero())

    @Test
    fun alContainsZero21() = assertTrue(list2.containsZero1())

    @Test
    fun alContainsZero22() = assertTrue(list2.containsZero2())

}
