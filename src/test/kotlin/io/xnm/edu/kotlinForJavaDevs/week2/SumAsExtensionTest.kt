package io.xnm.edu.kotlinForJavaDevs.week2

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class SumAsExtensionTest {

    @Test
    fun sum6() = assertEquals(6, listOf(1,2,3).sum())
}
