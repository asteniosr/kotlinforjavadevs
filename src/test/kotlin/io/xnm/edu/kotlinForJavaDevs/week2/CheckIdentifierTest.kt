package io.xnm.edu.kotlinForJavaDevs.week2

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class CheckIdentifierTest {

    private val sut = CheckIdentifier()

    @Test
    fun testName() = assertTrue(sut.isValidIdentifier("name"))   // true

    @Test
    fun testUName() = assertTrue(sut.isValidIdentifier("_name"))  // true

    @Test
    fun testU12() = assertTrue(sut.isValidIdentifier("_12"))    // true

    @Test
    fun testEmpty() = assertFalse(sut.isValidIdentifier(""))       // false

    @Test
    fun test012() = assertFalse(sut.isValidIdentifier("012"))    // false

    @Test
    fun testSymbol() = assertFalse(sut.isValidIdentifier("no$"))

    @Test
    fun testUpperCaseStart() = assertFalse(sut.isValidIdentifier("Ao1"))

    @Test
    fun testUpperCase() = assertTrue(sut.isValidIdentifier("aAAA"))

}
